# Docker PHP CMS Application

The project contains the ECS infrastructure as code to deploy a containerized PHP Wordpresss solution using AWS container service (ECS). 

## Prerequisites
* The ECS solution requires that an ECR resource is deployed and the app  image is built from the  [Docker-WordPress](https://gitlab.com/junydania/php-wordpress) template repository.
* Also a bucket must be 
* Deploy ECR resource in AWS
* Setup repository with  [Docker-WordPress](https://gitlab.com/junydania/php-wordpress) template
* Ensure a domain name configured in Route53 is setup
* Provide your AWS secret key and access key as environment variable in Gitlab CI/CD settings to grant Gitlab runner access to upload the templates to an S3 bucket.

## Architecture
![Architectural Diagram](sentia-wordpress.png)

### Components Overview

This project leverages the following services:

* [CloudFormation](https://aws.amazon.com/cloudformation/): Used to deploy the entire stack.
* [AWS Lamnda](https://docs.aws.amazon.com/lambda/index.html): Used to deploy SSL certificate
* [S3](https://aws.amazon.com/s3/): Used to store uploads
* [EC2](https://aws.amazon.com/ec2/): Used to deploy Gluster file system and docker containers.
* [ECS](https://aws.amazon.com/ecs/): Used for container orchestration
* [ACM] (https://aws.amazon.com/acm/): Used in creating certificate for SSL 
* [RDS] (https://aws.amazon.com/acm/): RDS as database for WordPress application

## CloudFormation

Cloudformation nested stack is used in deploying the infrastructure. The following templates are leveraged to deploy the WordPress service

| Template | Order of Execution | Description | Dependencies |
| -------- | :-----------------: | ----------- | ------------ |
| `main.yml` | 0 | Main Template to create ECS, RDS, and Storage resources | None |
| `vpc.yml` | 1 |  Template to deploy the VPC for the application. | main.yml |
| `ssl_certficate.yml` | 2 |  Template to deploy ssl certificates. | vpc.yml |
| `securitygroups.yml` | 3 | Template to create security groups | vpc.yml |
| `storage.yml` | 4 | Nested Template to create persistent storage for Wordpress file storage | securitygroups.yml |
| `rds.yml` | 5 | Nested Template to create database solution for Wordpress | securitygroups.yml |
| `ecs.yml` | 6 | Nested template to deploy ECS and Loadbalancing services | storage.yml, securitygroups.yml |
| `wordpress.yml` | 7 |  Template to deploy the Wordpress service to ECS. | ecs.yml |



# Pipeline Details
* Gitlab ci file validates the template and runs security scan on the cloudformation template
* Templates are pushed into an S3 bucket defined as a variable in the `.gitlab-ci.yml` file
* When deploying the infrastructure, required parameters are all specified in the `main.yml` file.

