---
AWSTemplateFormatVersion: '2010-09-09'
Description: 'VPC Network for Wordpress App'

Parameters:
  
  pCidr:
    Description: 'VPC CIDR Block'
    Type: String
    Default: "10.0.0.0/16"

  pSubnetPublicA:
    Description: 'VPC CIDR Block'
    Type: String
    Default: "10.0.0.0/20"

  pSubnetPublicB:
    Description: 'VPC CIDR Block'
    Type: String
    Default: "10.0.16.0/20"
  
  pSubnetPrivateA:
    Description: 'VPC CIDR Block'
    Type: String
    Default: "10.0.32.0/20"

  pSubnetPrivateB:
    Description: 'VPC CIDR Block'
    Type: String
    Default: "10.0.48.0/20"
    
  pSolutionName:
    Type: String
    Description: >
      The Solution name is the value used to reference the deployed solution. The
      value is used for export values to reference when deploying ECS services
      in separate CloudFormation templates to import the values created in the 
      supporting service stacks.

  pVPCName:
    Description: 'VPC Name'
    Type: String
    Default: "wordpress-vpc"


Mappings:

  AZRegions:
    ap-northeast-1:
      AZs: ["a", "b"]
    ap-northeast-2:
      AZs: ["a", "b"]
    ap-south-1:
      AZs: ["a", "b"]
    ap-southeast-1:
      AZs: ["a", "b"]
    ap-southeast-2:
      AZs: ["a", "b"]
    ca-central-1:
      AZs: ["a", "b"]
    eu-central-1:
      AZs: ["a", "b"]
    eu-west-1:
      AZs: ["a", "b"]
    eu-west-2:
      AZs: ["a", "b"]
    sa-east-1:
      AZs: ["a", "b"]
    us-east-1:
      AZs: ["a", "b"]
    us-east-2:
      AZs: ["a", "b"]
    us-west-1:
      AZs: ["a", "b"]
    us-west-2:
      AZs: ["a", "b"]


Resources:

  rVPC:
    Type: 'AWS::EC2::VPC'
    Properties:
      CidrBlock: !Ref pCidr
      EnableDnsSupport: true
      EnableDnsHostnames: true
      InstanceTenancy: default
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Name"
          Value:
            Ref: "AWS::StackName"

  rSubnetAPublic:
    Type: 'AWS::EC2::Subnet'
    Properties:
      AvailabilityZone: !Select [0, !GetAZs '']
      CidrBlock: !Ref pSubnetPublicA
      MapPublicIpOnLaunch: true
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Network"
          Value: "Public"
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-public-'
              - !Select [ 0, !FindInMap [ "AZRegions", !Ref "AWS::Region", "AZs" ] ]
  
  rSubnetBPublic:
    Type: 'AWS::EC2::Subnet'
    Properties:
      AvailabilityZone: !Select [1, !GetAZs '']
      CidrBlock: !Ref pSubnetPublicB
      MapPublicIpOnLaunch: true
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Network"
          Value: "Public"
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-public-'
              - !Select [ 1, !FindInMap [ "AZRegions", !Ref "AWS::Region", "AZs" ] ]

  rSubnetAPrivate:
    Type: 'AWS::EC2::Subnet'
    Properties:
      AvailabilityZone: !Select [0, !GetAZs '']
      CidrBlock: !Ref pSubnetPrivateA
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Network"
          Value: "Private"
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-private-'
              - !Select [ 0, !FindInMap [ "AZRegions", !Ref "AWS::Region", "AZs" ] ]

  rSubnetBPrivate:
    Type: 'AWS::EC2::Subnet'
    Properties:
      AvailabilityZone: !Select [1, !GetAZs '']
      CidrBlock: !Ref pSubnetPrivateB
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Network"
          Value: "Private"
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-private-'
              - !Select [ 1, !FindInMap [ "AZRegions", !Ref "AWS::Region", "AZs" ] ]

  rInternetGateway:
    Type: "AWS::EC2::InternetGateway"
    Properties:
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Network"
          Value: "Public"
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-IGW'

  rVPCGatewayAttachment:
    Type: 'AWS::EC2::VPCGatewayAttachment'
    Properties:
      VpcId: !Ref rVPC
      InternetGatewayId: !Ref rInternetGateway


  rRouteTablePublic:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Network"
          Value: "Public"
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-public-route-table'

  rPublicRoute:
    Type: "AWS::EC2::Route"
    DependsOn: "rInternetGateway"
    Properties:
      RouteTableId: !Ref rRouteTablePublic
      DestinationCidrBlock: "0.0.0.0/0"
      GatewayId: !Ref rInternetGateway

  rRouteTableAssociationAPublic:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref rSubnetAPublic
      RouteTableId: !Ref rRouteTablePublic

  rRouteTableAssociationBPublic:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref rSubnetBPublic
      RouteTableId: !Ref rRouteTablePublic

  rNetworkAclPublic:
    Type: 'AWS::EC2::NetworkAcl'
    Properties:
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Application"
          Value:
            Ref: "AWS::StackName"
        -
          Key: "Network"
          Value: "Public"
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-public-nacl'

  rNetworkAclEntryInPublicAllowAll:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref rNetworkAclPublic
      RuleNumber: 99
      Protocol: -1
      RuleAction: allow
      Egress: false
      CidrBlock: '0.0.0.0/0'

  rNetworkAclEntryOutPublicAllowAll:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref rNetworkAclPublic
      RuleNumber: 99
      Protocol: -1
      RuleAction: allow
      Egress: true
      CidrBlock: '0.0.0.0/0'


  rSubnetNetworkAclAssociationAPublic:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref rSubnetAPublic
      NetworkAclId: !Ref rNetworkAclPublic

  rSubnetNetworkAclAssociationBPublic:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref rSubnetBPublic
      NetworkAclId: !Ref rNetworkAclPublic

  rElasticIP0:
    Type: "AWS::EC2::EIP"
    Properties:
      Domain: "vpc"

  rElasticIP1:
    Type: "AWS::EC2::EIP"
    Properties:
      Domain: "vpc"

  rNATGateway0:
    Type: "AWS::EC2::NatGateway"
    Properties:
      AllocationId:
        Fn::GetAtt:
          - "rElasticIP0"
          - "AllocationId"
      SubnetId: !Ref rSubnetAPublic

  rNATGateway1:
    Type: "AWS::EC2::NatGateway"
    Properties:
      AllocationId:
        Fn::GetAtt:
          - "rElasticIP1"
          - "AllocationId"
      SubnetId: !Ref rSubnetBPublic

  rRouteTableAPrivate: 
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-private-route-table-0'

  rRouteTableBPrivate:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref rVPC
      Tags:
        -
          Key: "Name"
          Value: !Join
            - ''
            - - !Ref "pVPCName"
              - '-private-route-table-1'

  rPrivateRouteToInternet0:
    Type: "AWS::EC2::Route"
    Properties:
      RouteTableId: !Ref rRouteTableAPrivate
      DestinationCidrBlock: "0.0.0.0/0"
      NatGatewayId: !Ref rNATGateway0

  rPrivateRouteToInternet1:
    Type: "AWS::EC2::Route"
    Properties:
      RouteTableId: !Ref rRouteTableBPrivate
      DestinationCidrBlock: "0.0.0.0/0"
      NatGatewayId:
        Ref: "rNATGateway1"

  rRouteTableAssociationAPrivate:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref rSubnetAPrivate
      RouteTableId: !Ref rRouteTableAPrivate

  rRouteTableAssociationBPrivate:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref rSubnetBPrivate
      RouteTableId: !Ref rRouteTableBPrivate
      
  rNetworkAclPrivate:
    Type: 'AWS::EC2::NetworkAcl'
    Properties:
      VpcId: !Ref rVPC

  rSubnetNetworkAclAssociationAPrivate:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref rSubnetAPrivate
      NetworkAclId: !Ref rNetworkAclPrivate

  rSubnetNetworkAclAssociationBPrivate:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref rSubnetBPrivate
      NetworkAclId: !Ref rNetworkAclPrivate

  rNetworkAclEntryInPrivateAllowVPC:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref rNetworkAclPrivate
      RuleNumber: 99
      Protocol: -1
      RuleAction: allow
      Egress: false
      CidrBlock: '0.0.0.0/0'
      
  rNetworkAclEntryOutPrivateAllowVPC:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref rNetworkAclPrivate
      RuleNumber: 99
      Protocol: -1
      RuleAction: allow
      Egress: true
      CidrBlock: '0.0.0.0/0'

Outputs:

  oCidrBlock:
    Description: 'The set of IP addresses for the VPC.'
    Value: !GetAtt 'rVPC.CidrBlock'
    Export:
      Name: !Sub '${pSolutionName}-CidrBlock'

  oVPC:
    Description: 'VPC.'
    Value: !Ref rVPC
    Export:
      Name: !Sub '${pSolutionName}-VPC'

  oSubnetsPublic:
    Description: 'Subnets public.'
    Value: !Join [',', [!Ref rSubnetAPublic, !Ref rSubnetBPublic]]
    Export:
      Name: !Sub '${pSolutionName}-SubnetsPublic'

  oSubnetsPrivate:
    Description: 'Subnets private.'
    Value: !Join [',', [!Ref rSubnetAPrivate, !Ref rSubnetBPrivate]]
    Export:
      Name: !Sub '${pSolutionName}-SubnetsPrivate'

  oSubnetAPublic:
    Description: 'Subnet A public.'
    Value: !Ref rSubnetAPublic
    Export:
      Name: !Sub '${pSolutionName}-SubnetAPublic'

  oSubnetAPrivate:
    Description: 'Subnet A private.'
    Value: !Ref rSubnetAPrivate
    Export:
      Name: !Sub '${pSolutionName}-SubnetAPrivate'

  oSubnetBPublic:
    Description: 'Subnet B public.'
    Value: !Ref rSubnetBPublic
    Export:
      Name: !Sub '${pSolutionName}-SubnetBPublic'

  oSubnetBPrivate:
    Description: 'Subnet B private.'
    Value: !Ref rSubnetBPrivate
    Export:
      Name: !Sub '${pSolutionName}-SubnetBPrivate'