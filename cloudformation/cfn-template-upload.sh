#!/usr/bin/env bash

set -x -e

S3_URL=${1:-s3://sentia-wordpress-test}

# upload zip file
aws s3 sync infrastructure-code $S3_URL/ --exclude "params.json"