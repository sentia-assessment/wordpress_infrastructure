#!/usr/bin/env bash

set -x -e

LAMBDA_BUCKET_NAME=$1
S3_URL="s3://${LAMBDA_BUCKET_NAME}"

TMP_ZIP=$TMPDIR/lambda_functions.zip

cd lambda/

pip3 install boto3 -t ./

chmod -R 755 .

# if tmp file exists, delete it
if [ -e $TMP_ZIP ]; then rm $TMP_ZIP; fi

# create zip
zip -v -r $TMP_ZIP *.py requests

# upload zip file
aws s3 cp $TMP_ZIP $S3_URL/